module Instagram
  CONFIG = YAML.load_file(Rails.root.join("config/instagram.yml"))[Rails.env]
  APP_ID = CONFIG["app_id"]
  SECRET = CONFIG["app_secret"]
end

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :instagram, Instagram::APP_ID, Instagram::SECRET
end

OmniAuth.config.on_failure = UsersController.action(:omniauth_failure)