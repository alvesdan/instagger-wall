InstaggerWall::Application.routes.draw do
  
  root to: "home#index"
  get "/home", to: "home#index", as: :home
  get "/auth/instagram/callback", to: "users#create"
  get "/auth/signout", to: "sessions#destroy"
  get "/auth/authenticate", to: "sessions#show"
  resources :walls, only: [:new, :create] do
    resources :removed_contents, only: [:index, :create]
  end
  get "/:wall_url", to: "walls#show", as: :wall
  
end
