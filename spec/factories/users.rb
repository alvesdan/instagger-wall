# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "userteste#{n}" }
    sequence(:instagram_id) { |n| n }
    sequence(:instagram_token) { |n| "token#{n}" }
    sequence(:instagram_nickname) { |n| "userteste#{n}" }
  end
end
