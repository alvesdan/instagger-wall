# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :wall do
    sequence(:hashtag) { |n| "hashtag#{n}" }
  end
end
