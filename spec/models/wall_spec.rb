require 'spec_helper'

describe Wall do
  
  describe :associations do
    it { should belong_to(:user) }
    it { should have_many(:removed_contents).dependent(:destroy) }
  end
  
  describe :validations do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:hashtag) }
    context :hashtag do
      it { should allow_value("jackdaniels").for(:hashtag) }
      it { should_not allow_value("#jackdaniels").for(:hashtag) }
      it { should_not allow_value("jack daniels").for(:hashtag) }
    end
  end
  
  context "from user" do
    
    let(:user) {
      FactoryGirl.create(:user)
    }
    
    it "should generate url" do
      wall = FactoryGirl.build(:wall, user: user)
      expect { wall.save! }.to change(wall, :url).from(nil).to(/\A\S+\z/)
    end
    
  end
  
end
