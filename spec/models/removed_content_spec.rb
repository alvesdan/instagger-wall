require 'spec_helper'

describe RemovedContent do
  
  it "should validates removed content" do
    removed_content = RemovedContent.new
    expect(removed_content).to have(1).error_on(:wall)
    expect(removed_content).to have(1).error_on(:content_id)
  end
  
  
end
