require 'spec_helper'

describe User do
  
  describe :associations do
    it { should have_many(:walls) }
  end
  
  describe :validations do
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:instagram_id) }
    it { should validate_presence_of(:instagram_token) }
    it { should validate_presence_of(:instagram_nickname) }
  end
    
end
