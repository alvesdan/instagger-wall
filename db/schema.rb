# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131128043108) do

  create_table "removed_contents", force: true do |t|
    t.integer  "wall_id"
    t.string   "content_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "removed_contents", ["wall_id", "content_id"], name: "index_removed_contents_on_wall_id_and_content_id", unique: true
  add_index "removed_contents", ["wall_id"], name: "index_removed_contents_on_wall_id"

  create_table "users", force: true do |t|
    t.string   "username"
    t.integer  "instagram_id"
    t.string   "instagram_token"
    t.string   "instagram_nickname"
    t.string   "instagram_image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["instagram_id"], name: "index_users_on_instagram_id", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

  create_table "walls", force: true do |t|
    t.integer  "user_id"
    t.string   "hashtag",    null: false
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "walls", ["hashtag"], name: "index_walls_on_hashtag", unique: true
  add_index "walls", ["url"], name: "index_walls_on_url", unique: true
  add_index "walls", ["user_id"], name: "index_walls_on_user_id"

end
