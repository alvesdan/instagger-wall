class CreateWalls < ActiveRecord::Migration
  def change
    create_table :walls do |t|
      t.belongs_to :user, index: true
      t.string :hashtag, null: false
      t.string :url
      t.timestamps
    end
    
    add_index :walls, :hashtag, unique: true
    add_index :walls, :url, unique: true
    
  end
end
