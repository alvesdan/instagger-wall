class CreateRemovedContents < ActiveRecord::Migration
  def change
    create_table :removed_contents do |t|
      t.belongs_to :wall, index: true
      t.string :content_id
      t.timestamps
    end
    
    add_index :removed_contents, [:wall_id, :content_id], unique: true
    
  end
end
