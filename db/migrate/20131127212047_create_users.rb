class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.integer :instagram_id
      t.string :instagram_token
      t.string :instagram_nickname
      t.string :instagram_image
      t.timestamps
    end
    
    add_index :users, :username, unique: true
    add_index :users, :instagram_id, unique: true
    
  end
end
