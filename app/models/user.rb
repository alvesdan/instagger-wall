class User < ActiveRecord::Base
  
  validates :username, :instagram_id, :instagram_token, :instagram_nickname, presence: true
  validates :username, :instagram_id, :instagram_nickname, uniqueness: true
  
  has_many :walls, inverse_of: :user
  
end
