require "base62"
class Wall < ActiveRecord::Base
  
  belongs_to :user, inverse_of: :walls
  has_many :removed_contents, inverse_of: :wall, dependent: :destroy
  
  validates :user, :hashtag, presence: true
  validates :hashtag, uniqueness: true
  validates :hashtag, length: { in: 4..30 }
  validates :hashtag, format: /\A[a-zA-z0-9]+\z/
  
  after_create :generate_url
  
  def self.find_by_url(wall_url)
    find_by(url: "#{wall_url.base62_decode}")
  end
  
  def generate_url
    update_attribute(:url, "#{self.id.base62_encode}")
  end
  
end
