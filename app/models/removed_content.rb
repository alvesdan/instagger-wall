class RemovedContent < ActiveRecord::Base
  
  belongs_to :wall, inverse_of: :removed_contents  
  validates :wall, :content_id, presence: true
  
end
