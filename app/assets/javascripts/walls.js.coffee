class Wall
  constructor: ->
    @wall_id = parseInt($("#wall_id").val())
    @tag = $("#tag").val()
    @token = $("#token").val()
    @removed_content = $("#removed_content").val().split(",")
    url = "https://api.instagram.com/v1/tags/#{@tag}/media/recent?access_token=#{@token}"
    @loadMedia(url)
    @bindEvents()
  
  loadMedia: (url) ->
    _this = this
    _this.addLoadingClass()
    $.ajax
      url: url
      type: "get"
      dataType: "jsonp"
      success: (response) ->
        console.log response
        if response.data.length
          _this.removeLoadMore()
          _this.parseMedia(response)
          if response.pagination.next_url
            if response.data.length < 9
              _this.loadMedia(response.pagination.next_url)
              return
            $(".wall ul.holder").after("<a href=\"#\" data-url=\"#{response.pagination.next_url}\" class=\"load-more\" data-original-title=\"Load more\"><span class=\"glyphicon glyphicon-circle-arrow-down\"></span></a>")
            $(".load-more").tooltip
              placement: "top"
              container: "body"
        else
          $(".empty-wall .reload").css "display", "block"
  
  addLoadingClass: ->
    $(".load-more").addClass("loading")
  
  removeLoadMore: ->
    $(".load-more").remove()
    
  shouldSkipObject: (id) ->
    for media_id in @removed_content
      console.log media_id
      return true if media_id is id
    return false
  
  parseMedia: (media) ->
    _this = this
    for object in media.data
      continue if @shouldSkipObject(object.id)
      if object.type is "image"
        template = "<li class=\"col-lg-3 col-sm-4 col-md-4\" id=\"photo-#{object.id}\">
          <div class=\"photo clearfix\">
            <img src=\"#{object.images.low_resolution.url}\" />
            <div class=\"information clearfix\">
              <span class=\"author\">#{@authorName(object)}</span>
              <span class=\"links\">
                <a href=\"#\" data-original-title=\"Remove from wall\" class=\"text-danger\" data-id=\"#{object.id}\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                <a href=\"#{object.link}\" target=\"_blank\" data-original-title=\"See in Instagram\"><span class=\"glyphicon glyphicon-new-window\"></span></a>
              </span>
            </div>
          </div>
        </li>"
        $(".wall ul.holder").append(template)
        $(".wall ul.holder > li#photo-#{object.id}").addClass("display")
        if window.userSession.loggedIn
          $(".wall ul.holder > li#photo-#{object.id} a.text-danger").click ->
            id = $(this).data("id")
            that = this
            $.ajax
              url: "/walls/#{_this.wall_id}/removed_contents"
              type: "post"
              dataType: "script"
              data: { removed_content: { content_id: id }}
              success: ->
                $(that).tooltip("destroy")
            return false
        else
          $(".wall ul.holder > li#photo-#{object.id} a.text-danger").remove()
        $(".wall ul.holder > li .information .links a").tooltip
          placement: "top"
          container: "body"
    @rotatePhotos()
  
  rotatePhotos: ->
    rotations = [-2.5, -2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2, 2.5]
    $(".wall ul.holder > li").each ->
      rotation = rotations[Math.floor((Math.random() * 7))]
      $(this).css
        "transform": "rotate(#{rotation}deg)"
        "-ms-transform": "rotate(#{rotation}deg)"
        "-webkit-transform": "rotate(#{rotation}deg)"
  
  bindEvents: ->
    _this = this
    $(document).on "click", ".load-more", (event) ->
      $(this).tooltip("destroy")
      _this.loadMedia($(this).data("url"))
      return false
  
  authorName: (object) ->
    try
      object.caption.from?.full_name?.replace?(/[^a-zA-Z0-9]/, "")
    catch
      "-"

$ ->
  wall = new Wall() if $("section.wall").length