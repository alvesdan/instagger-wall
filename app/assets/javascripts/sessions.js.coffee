class UserSession
  constructor: ->
    @loggedIn = false
    _this = this
    $.ajax
      url: "/auth/authenticate"
      type: "get"
      dataType: "json"
      success: (response) ->
        if response.id isnt null
          _this.loggedIn = true
          $("header .btn-group").append("<a href=\"/auth/signout\" class=\"btn btn-sm\"><span class=\"glyphicon glyphicon-off\"></span></a>")

$ ->
  window.userSession = new UserSession()  