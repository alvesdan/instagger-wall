$ ->
  if $("#modal-privacy-policy").length
    $("#modal-privacy-policy").modal(show: false)
    $("#modal-about").modal(show: false)
    
    $(".about").on "click", ->
      $("#modal-about").modal("show")
      false
    
    $(".privacy-policy").on "click", ->
      $("#modal-privacy-policy").modal("show")
      false
    