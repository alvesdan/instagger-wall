module Cryptor
  
  def encryptor
    secret = "aWTvbXMhZCBvhW1kc2FvaSBtZGFzb2kaZG1hc29pZG0gYXNvaWRtIG9haXNkbSBhZA=="
    ActiveSupport::MessageEncryptor.new(secret)
  end
  
  def encrypt_string(string)
    encryptor.encrypt_and_sign(string)
  end
  
  def decrypt_string(string)
    encryptor.decrypt_and_verify(string) rescue nil
  end
  
end