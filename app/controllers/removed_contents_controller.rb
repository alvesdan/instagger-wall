class RemovedContentsController < ApplicationController
  
  before_action :load_wall
  
  def index
    respond_to do |format|
      format.json {
        render json: @wall.removed_contents.all
      }
    end
  end
  
  def create
    @removed_content = @wall.removed_contents.create(removed_content_params)
    respond_to do |format|
      format.js
    end
  end
  
  private
  
  def load_wall
    @wall = current_user.walls.find(params[:wall_id])
  end
  
  def removed_content_params
    params.require(:removed_content).permit(:content_id)
  end
  
end