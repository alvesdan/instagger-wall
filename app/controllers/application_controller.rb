class ApplicationController < ActionController::Base
  
  include Cryptor
  protect_from_forgery with: :exception
  
  def sign_in(user)
    cookies[:user_token] = { value: encrypt_string(user.id) } 
  end
  
  def sign_out
    cookies.delete :user_token
  end
  
  def current_user
    @current_user ||= User.find(decrypt_string(cookies[:user_token])) if cookies[:user_token].present?
  end
  helper_method :current_user
  
  def authenticate_user!
    redirect_to root_path unless current_user
  end
  
end
