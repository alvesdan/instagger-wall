class UsersController < ApplicationController
  
  def create
    auth = request.env["omniauth.auth"]
    @user = User.find_or_create_by({
      username: auth["info"]["nickname"],
      instagram_id: auth["uid"],
      instagram_token: auth["credentials"]["token"],
      instagram_nickname: auth["info"]["nickname"],
      instagram_image: auth["info"]["image"]
    })
    if @user.persisted?
      sign_in(@user)
      redirect_to new_wall_path
    else
      redirect_to root_path
    end
  end
  
  def omniauth_failure
    if params[:error_reason] === "user_denied"
      flash[:notice] = "You must authorize our app to access your Instagram account"
    end
    redirect_to roo_path
  end
  
end