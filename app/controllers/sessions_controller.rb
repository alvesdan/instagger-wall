class SessionsController < ApplicationController
  
  def show
    respond_to do |format|
      format.json {
        render json: { id: current_user.try(:id) }
      }
    end
  end
  
  def destroy
    sign_out
    redirect_to root_path
  end
  
end