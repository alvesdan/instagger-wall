class WallsController < ApplicationController
  
  before_action :authenticate_user!, only: [:new, :create]
  
  def new
    @walls = current_user.walls
    @wall = current_user.walls.new
  end
  
  def create
    @wall = current_user.walls.create(wall_params)
    respond_to do |format|
      format.js {
        @wall.reload if @wall.persisted?
      }
    end
  end
  
  def show
    @wall = Wall.find_by(url: params[:wall_url])
    @pagetitle = "Instagger Wall ##{@wall.hashtag}"
    @pagedescription = "Invite your friends or take a picture!"
    redirect_to root_path unless @wall
  end
  
  protected
  
  def wall_params
    params.require(:wall).permit(:hashtag)
  end
  
end