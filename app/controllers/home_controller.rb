class HomeController < ApplicationController
  
  def index
    redirect_to new_wall_path if current_user
  end
  
end