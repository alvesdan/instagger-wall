module ApplicationHelper
  
  def icon_tag(icon, content = nil)
    content_tag(:span, nil, class: "glyphicon #{icon}") + " #{content}"
  end
  
end
